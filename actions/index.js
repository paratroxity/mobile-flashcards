export const RECEIVE_DECK_ENTRIES = 'RECEIVE_DECK_ENTRIES'
export const ADD_DECK = 'ADD_DECK'

export const RECEIVE_CARD_ENTRIES = 'RECEIVE_CARD_ENTRIES'
export const ADD_CARD = 'ADD_CARD'


export function receiveDeckEntries (entries) {
  return {
    type: RECEIVE_DECK_ENTRIES,
    entries,
  }
}

export function addDeck ({name,cardCount},nextId) {
  return {
    type: ADD_DECK,
    name,
    cardCount,
    nextId
  }
}

export function receiveCardEntries (entries) {
  return{
    type: RECEIVE_CARD_ENTRIES,
    entries
  } 
}

export function addCard ({question,answer,deckId},nextId){
  return{
    type: ADD_CARD,
    question,
    answer,
    deckId,
    nextId
  }
}