import React, {Component} from 'react'
import {Text,View,StyleSheet,ScrollView,TouchableOpacity,Dimensions,Animated} from 'react-native'
import FlipCard from 'react-native-flip-card'


class Card extends Component{
    state={back:false}

    showFront(){
        this.setState({back:false})
    }

    showBack(){
        this.setState({back:true})
    }

    render(){
        const {question,answer,incrementCard,totalCards,currentCard} = this.props
        return(
            <View style={styles.container}>
                <FlipCard clickable={false} flip={this.state.back}  perspective={500}>
                    <View style={[styles.card]}>
                        <Text style={{alignSelf:'flex-start'}}> {currentCard} / {totalCards}</Text>
                        <Text style={{fontSize:34,marginTop:'auto',textAlign:'center'}}>{question}</Text>
                        <TouchableOpacity onPress={()=>this.showBack()}><Text style={{color:'red',fontSize:16}}>Answer</Text></TouchableOpacity>
                        <TouchableOpacity style={[styles.btn,{marginTop:'auto',backgroundColor:'green',marginBottom:7}]} onPress={()=>incrementCard(true)}>
                            <Text style={{color:'white'}}>Correct</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.btn,{backgroundColor:'red',marginBottom:50}]} onPress={()=>incrementCard(false)}>
                            <Text style={{color:'white'}}>Incorrect</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={[styles.card]}>
                        <Text style={{alignSelf:'flex-start'}}> {currentCard} / {totalCards}</Text>
                        <Text style={{fontSize:34,marginTop:'auto',textAlign:'center'}}>{answer}</Text>
                        <TouchableOpacity onPress={()=>this.showFront()}><Text style={{color:'red',fontSize:16}}>Question</Text></TouchableOpacity>
                        <TouchableOpacity style={[styles.btn,{marginTop:'auto',backgroundColor:'green',marginBottom:7}]} onPress={()=>incrementCard(false)}>
                            <Text style={{color:'white'}}>Correct</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.btn,{backgroundColor:'red',marginBottom:50}]} onPress={()=>incrementCard(false)}>
                            <Text style={{color:'white'}}>Incorrect</Text>
                        </TouchableOpacity>
                    </View>
                </FlipCard>
            </View>

        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex:1,
        backgroundColor: '#fff',
    },
    card:{
        flex:1,
        alignItems:'center',
        
    },
    btn:{
        width: Dimensions.get('window').width*0.6,
        borderWidth:1,
        paddingTop:13,
        borderRadius: 6,
        height: 45,
        paddingLeft: 50,
        paddingRight: 50,
        borderColor:'black',
        alignItems:'center'
    }
}); 

export default Card