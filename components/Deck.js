import React, {Component} from 'react'
import {Text,View,StyleSheet,ScrollView,TouchableOpacity,Dimensions} from 'react-native'
import { connect } from 'react-redux'

class Deck extends Component{

    static navigationOptions = ({navigation}) => {
        const {deckName} = navigation.state.params
        return{
            title: deckName
        }
    }
    render(){
        const {navigation,decks} = this.props
        const {deckId} = navigation.state.params
        cardCount = decks[deckId].cardCount
        deckName = decks[deckId].name
        return(
            <View style={styles.container}>
                <Text style={{fontWeight:'bold',fontSize:34,marginTop:'auto'}}>{deckName}</Text>
                <Text style={{color:'grey',fontSize:16}}>{cardCount} cards</Text>
                <TouchableOpacity style={[styles.btn,{marginTop:'auto',backgroundColor:'white',marginBottom:7}]} onPress={()=>navigation.navigate(
                    'NewCardView',{deckId})}>
                    <Text style={{color:'black'}}>Add Card</Text>
                </TouchableOpacity>
                <TouchableOpacity disabled={true ? cardCount === 0 : false} style={[styles.btn,{backgroundColor:'black',marginBottom:50}]} onPress={()=>navigation.navigate(
                    'QuizView',{deckId})}>
                    <Text style={{color:'white'}}>Start Quiz</Text>
                </TouchableOpacity>
            </View>
        )
    }


}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems:'center'
    },
    btn:{
        width: Dimensions.get('window').width*0.6,
        borderWidth:1,
        paddingTop:13,
        borderRadius: 6,
        height: 45,
        paddingLeft: 50,
        paddingRight: 50,
        borderColor:'black',
        alignItems:'center'
    }
  });


  function mapStateToProps({deckEntries}){
    return{
        decks:{
            ...deckEntries
        }
    }

}

export default connect(mapStateToProps,)(Deck)