import React, {Component} from 'react'
import {Text,View,StyleSheet,ScrollView,TouchableOpacity} from 'react-native'
import {receiveDeckEntries} from '../actions'
import { connect } from 'react-redux'
import {fetchDeckResults} from '../utils/api'
import { AppLoading} from 'expo'

class DeckList extends Component{
    state = {ready:false}

    componentDidMount(){
        const {dispatch} = this.props
        fetchDeckResults().then((results)=>dispatch(receiveDeckEntries(results)))
        .then(()=>this.setState({ready:true}))
    }

    render(){
        const {ready} = this.state;
        const {decks} = this.props;
        if(!ready)
            return(<AppLoading/>)
        else
        return(
            <ScrollView style={styles.container}>
                {Object.keys(decks).map((deckId) => (
                    <TouchableOpacity style={styles.deckBox} key={deckId} onPress={() => this.props.navigation.navigate(
                        'DeckView',
                        {deckId,deckName:decks[deckId].name})}>
                        <Text style={{fontSize:24}}>{decks[deckId].name}</Text>
                        <Text style={{color:'grey'}}>{decks[deckId].cardCount} cards</Text>
                    </TouchableOpacity>
                ))}
            </ScrollView>
        )
    }


}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff'
    },
    deckBox:{
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomWidth: StyleSheet.hairlineWidth,
        borderBottomColor: 'black',
        borderTopWidth: StyleSheet.hairlineWidth,
        borderTopColor: 'black',
        borderRadius: 0,
        padding:40
    }
  });


function mapStateToProps({deckEntries}){
    return{
        decks:{
            ...deckEntries
        }
    }

}

export default connect(mapStateToProps,)(DeckList)