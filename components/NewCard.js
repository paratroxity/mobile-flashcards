import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Text,View,StyleSheet,ScrollView,TouchableOpacity,KeyboardAvoidingView,TextInput,Dimensions} from 'react-native'
import {submitCard} from '../utils/api'
import {addCard} from '../actions/index'

class NewCard extends Component{
    state={
        question:'',
        answer:''
    }

    handleCardSubmit = ()=>{
        const {question,answer} = this.state;
        const {dispatch,navigation} = this.props;
        const {deckId} = navigation.state.params;
        if(question.trim()===''){
            alert('Please enter a question.');
            return;
        }
        if(answer.trim()===''){
            alert('Please enter an answer.');
            return;
        }
        let newCardObj = {question,answer,deckId};
        submitCard(newCardObj).then((nextId)=>dispatch(addCard(newCardObj,nextId)));
        navigation.goBack();
    }

    render(){
        const{navigation} = this.props
        const {deckId} = navigation.state.params;
        return(
            <KeyboardAvoidingView style={styles.container} behavior='padding'>
                <TextInput placeholder='Enter quesiton here...' style={[styles.input,{marginTop:20}]} onChangeText={(value) => this.setState({question: value})}/>
                <TextInput placeholder='Enter answer here...' style={[styles.input,{marginTop:40}]} onChangeText={(value) => this.setState({answer: value})}/>
                <TouchableOpacity style={styles.btn} onPress={this.handleCardSubmit}><Text style={{color:'white'}}>Submit</Text></TouchableOpacity>
            </KeyboardAvoidingView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'flex-start',
        alignItems:'center'
    },
    input:{
        width:Dimensions.get('window').width*0.8,
        height:44,
        borderWidth:1,
        borderColor:'black',
        borderRadius:6,
        fontSize:12
    },
    btn:{
        width: Dimensions.get('window').width*0.4,
        padding:10,
        borderWidth:1,
        borderRadius: 6,
        borderColor:'black',
        alignItems:'center',
        backgroundColor:'black',
        marginTop:30

    }



})

export default connect(null,)(NewCard)