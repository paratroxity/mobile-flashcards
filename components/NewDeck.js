import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Text,View,StyleSheet,ScrollView,TouchableOpacity,KeyboardAvoidingView,TextInput,Dimensions} from 'react-native'
import {addDeck} from '../actions/index'
 import {submitDeck} from '../utils/api'

class NewDeck extends Component{
    state={
        title:''
    }

    handleTitleSubmit = () => {
        const {title} = this.state;
        const {dispatch,navigation} = this.props
        if(title.trim() !== ''){
            let newDeckObj = {name:title,cardCount:0}
            submitDeck(newDeckObj).then((nextId)=>dispatch(addDeck(newDeckObj,nextId))).then((obj)=>navigation.navigate('DeckView',{deckId:obj.nextId,deckName:title}));
        }else{
            alert('Please input a title.');
        }
    }

    render(){
        return(
            <KeyboardAvoidingView style={styles.container} behavior='padding'>
                <Text style={styles.text}>What is the title of your new deck?</Text>
                <TextInput placeholder='Enter title here...' style={[styles.input,{marginTop:40}]} onChangeText={(value) => this.setState({title: value})}/>
                <TouchableOpacity style={styles.btn} onPress={this.handleTitleSubmit}><Text style={{color:'white'}}>Submit</Text></TouchableOpacity>
            </KeyboardAvoidingView>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent:'flex-start',
        alignItems:'center'
    },
    text:{
        textAlign:'center',
        marginTop:25,
        fontSize:24
    },
    input:{
        width:Dimensions.get('window').width*0.8,
        height:44,
        borderWidth:1,
        borderColor:'black',
        borderRadius:6,
        fontSize:12
    },
    btn:{
        width: Dimensions.get('window').width*0.4,
        padding:10,
        borderWidth:1,
        borderRadius: 6,
        // height: 45,
        // paddingLeft: 50,
        // paddingRight: 50,
        borderColor:'black',
        alignItems:'center',
        backgroundColor:'black',
        marginTop:30

    }



})

export default connect(null,)(NewDeck)