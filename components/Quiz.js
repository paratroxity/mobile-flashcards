import React, {Component} from 'react'
import {connect} from 'react-redux'
import {Text,View,StyleSheet,ScrollView,TouchableOpacity,Dimensions} from 'react-native'
import {fetchCardResults} from '../utils/api'
import {receiveCardEntries} from '../actions/index'
import { AppLoading} from 'expo'
import Card from './Card'
import PercentageCircle from 'react-native-percentage-circle';
import {clearLocalNotification,setLocalNotification} from '../utils/helpers'

class Quiz extends Component{
    state = {ready:false,quizCards:null,activeCardIndex:null,numCorrect:0}
    
    incrementCard = (isCorrect) => {
        let {activeCardIndex,quizCards,numCorrect} = this.state
        if(activeCardIndex+1 >= Object.keys(quizCards).length){
            activeCardIndex = 'summary';
        }else{
            activeCardIndex = activeCardIndex + 1;
        }
        if(isCorrect){
            this.setState({numCorrect:numCorrect+1,activeCardIndex})
        }else{
            this.setState({activeCardIndex})
        }
    }

    resetQuiz = ()=>{
        this.setState({numCorrect:0,activeCardIndex:0});
    }

    componentDidMount(){
        const {quizCards} = this.state
        const {dispatch,receiveEntries,navigation} = this.props;
        const{deckId} = navigation.state.params;
        fetchCardResults().then((results)=>dispatch(receiveCardEntries(results)))
        .then(()=>{
            cards = this.props.cards;
            filteredCardObj = Object.keys(cards).filter((cardId)=>cards[cardId].deckId==deckId).reduce((result,key)=>{result[key]=cards[key];return result;},{})
            this.setState({quizCards:filteredCardObj,ready:true,activeCardIndex:0});
        });

    }

    render(){
        const {ready,quizCards,activeCardIndex,numCorrect} = this.state;
        const {cards,navigation} = this.props;
        const {deckId} = navigation.state.params;
        if(!ready)
            return(<AppLoading/>)   
        else {
            if(activeCardIndex==='summary'){
                // clearLocalNotification().then(setLocalNotification)
                return(
                    <View style={styles.container}>
                        <Text style={{fontSize:24,marginTop:10, marginBottom:10 ,textAlign:'center'}}>Percentage Correct</Text>
                        <PercentageCircle radius={55} percent={numCorrect/Object.keys(quizCards).length*100} color={"blue"}></PercentageCircle>
                        <TouchableOpacity style={[styles.btn,{marginTop:'auto',backgroundColor:'white',marginBottom:7}]} onPress={()=>this.resetQuiz()}>
                            <Text style={{color:'black'}}>Restart Quiz</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={[styles.btn,{backgroundColor:'black',marginBottom:50}]} onPress={()=>navigation.goBack()}>
                            <Text style={{color:'white'}}>Back To Deck</Text>
                        </TouchableOpacity>
                    </View>
                )
            } else{
                const cardObj = quizCards[Object.keys(quizCards)[activeCardIndex]];
                return(
                    <Card 
                    question={cardObj.question} 
                    answer={cardObj.answer} 
                    incrementCard={this.incrementCard} 
                    totalCards={Object.keys(quizCards).length} 
                    currentCard={activeCardIndex+1}
                    key={activeCardIndex}/>
                )
            }
    }
    }

}

const styles = StyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: '#fff',
      alignItems:'center'
    },
    btn:{
        width: Dimensions.get('window').width*0.6,
        borderWidth:1,
        paddingTop:13,
        borderRadius: 6,
        height: 45,
        paddingLeft: 50,
        paddingRight: 50,
        borderColor:'black',
        alignItems:'center'
    }
  });

function mapStateToProps({cardEntries}){
    return{
        cards:{
            ...cardEntries
        }
    }

}


export default connect(mapStateToProps,)(Quiz)