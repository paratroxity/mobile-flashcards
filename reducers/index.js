import { RECEIVE_DECK_ENTRIES, ADD_DECK, ADD_CARD,RECEIVE_CARD_ENTRIES } from '../actions'
import {combineReducers} from 'redux'

function deckEntries (state = {}, action) {
  switch (action.type) {
    case RECEIVE_DECK_ENTRIES :
      return {
        ...state,
        ...action.entries,
      }
    case ADD_DECK :
      const {name,cardCount,nextId} = action;
      return {
        ...state,
        [nextId]:{
          name,
          cardCount
        }
      }
    case ADD_CARD:
        const {deckId} = action;
        return{
          ...state,
          [deckId]:{
            ...state[deckId],
            cardCount: state[deckId]['cardCount'] + 1
          }
        }
    default:
      return state
  }
}


function cardEntries (state ={},action){
  switch(action.type){

    case RECEIVE_CARD_ENTRIES:
      return {
        ...state,
        ...action.entries
      }

    case ADD_CARD:
    const {question,answer,deckId,nextId} = action;
    return {
      ...state,
      [nextId]:{
        question,
        answer,
        deckId
      }
    }
    
    default:
      return state
  }
}

export default combineReducers({
  deckEntries,
  cardEntries
})