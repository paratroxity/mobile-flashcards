// Utilities for backfilling the calendar.

import { AsyncStorage } from 'react-native'

export const CARD_STORAGE_KEY = 'UdaciCards:card'

function setDummyData(){
    let placeholder = {}
    placeholder = {
        1:{
            question:'Whats the best number?',
            answer:5,
            deckId:1
        },
        2:{
            question:'Whats my least favorite number?',
            answer:2,
            deckId:1
        },
        3:{
            question:'Loooool I am...',
            answer:1,
            deckId:2
        },
        4:{
            question:'1+1',
            answer:2,
            deckId:1
        }
    }
    AsyncStorage.setItem(CARD_STORAGE_KEY,JSON.stringify(placeholder));
    return placeholder;
}

export function formatCardResults (results) {
    // return setDummyData();
  return results === null
    ? setDummyData()
    : JSON.parse(results)
}