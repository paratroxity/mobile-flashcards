// Utilities for backfilling the calendar.

import { AsyncStorage } from 'react-native'

export const DECK_STORAGE_KEY = 'UdaciCards:deck'

function setDummyData(){
    let placeholder = {}
    placeholder = {
        1:{
            name:'Chem',
            cardCount:3
        },
        2:{
            name:'History',
            cardCount:1
        },
        3:{
            name:'Compsci',
            cardCount:0
        },
        4:{
            name:'Psych',
            cardCount:0                
        },
        5:{
            name:'Chem',
            cardCount:0
        },
        6:{
            name:'History',
            cardCount:0
        },
        7:{
            name:'Compsci',
            cardCount:0
        },
        8:{
            name:'ML',
            cardCount:0                
        }
    }
    AsyncStorage.setItem(DECK_STORAGE_KEY,JSON.stringify(placeholder));
    return placeholder;
}

export function formatDeckResults (results) {
    // return setDummyData();
  return results === null
    ? setDummyData()
    : JSON.parse(results)
}