import { AsyncStorage } from 'react-native'
import { formatDeckResults, DECK_STORAGE_KEY } from './_deck'
import {formatCardResults, CARD_STORAGE_KEY} from './_card'

export function fetchDeckResults () {
  return AsyncStorage.getItem(DECK_STORAGE_KEY)
    .then(formatDeckResults)
}

export function fetchCardResults (){
  return AsyncStorage.getItem(CARD_STORAGE_KEY)
    .then(formatCardResults)
}

export function submitDeck (deck) {
  return AsyncStorage.getItem(DECK_STORAGE_KEY)
  .then(formatDeckResults).then((result)=>{
    let nextId = Math.max.apply(Math,Object.keys(result).map(Number)) + 1;
    AsyncStorage.mergeItem(DECK_STORAGE_KEY, JSON.stringify({
      [nextId]: deck
    }))
    return nextId;
  });
}

export function submitCard (card) {
  return AsyncStorage.getItem(CARD_STORAGE_KEY)
  .then(formatCardResults).then((result)=>{
    let nextId = Math.max.apply(Math,Object.keys(result).map(Number)) + 1;
    AsyncStorage.mergeItem(CARD_STORAGE_KEY, JSON.stringify({
      [nextId]: card
    }))
    AsyncStorage.getItem(DECK_STORAGE_KEY)
    .then((result)=>incrementCardCount(result,card))
    return nextId;
  });
}
    
function incrementCardCount(deckInfo,card){
  deckInfo = JSON.parse(deckInfo)
  newDeckObj = {...deckInfo[card.deckId],cardCount:deckInfo[card.deckId].cardCount+1}
  AsyncStorage.mergeItem(DECK_STORAGE_KEY, JSON.stringify({
    [card.deckId]: newDeckObj
  }))
}

